#!/usr/bin/python3
"""
pyiwd: A graphical frontend for iwd, Intel's iNet Wireless Daemon.
(c) 2021-2023 Johannes Willem Fernhout, BSD 3-Clause License applies.
"""

from . import iwdgui

iwdgui.main()
