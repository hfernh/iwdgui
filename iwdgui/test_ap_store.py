#!/usr/bin/env python3

"""
test_ap_store: test store and retrieve functions for ap profiles
(c) 2021-2023 Johannes Willem Fernhout, BSD 3-Clause License applies.

Usage: python -m unittest iwdgui.test_ap_store

"""
import time
import unittest

from . import ap_store

suffix = "-test-test-test-test"

class test_pyiwd_station(unittest.TestCase):

    def test_one(self):
        nws = []
        for num in range(3):
            nws.append({"Name" : str(num) + suffix,
                        "Passphrase" : "passphrase_" + str(num),
                        "AutoConnect" : True })
        for nw in nws:
            ap_store.store_ap_profile(nw)

        time.sleep(1)
        nws[1]["Passphrase"] = "11111111"
        ap_store.store_ap_profile(nws[1])
        nws2 = ap_store.read_ap_profiles()

        print("nws2:", nws2)




if __name__ == '__main__':
    unittest.main()


