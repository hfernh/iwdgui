#!/usr/bin/python3
"""
exitcodes: Just a list of exit codes in case something goes wrong
(c) 2021-2023 Johannes Willem Fernhout, BSD 3-Clause License applies.
"""

NORMAL_EXIT = 0
IMPORT_FAILED = 1
DBUS_ATTACH_ERROR = 2
IWD_NOT_RUNNING = 3
NO_DEVICES_DETECTED = 4
NO_DEVICES_IN_STATION_MODE = 5
INTERNAL_ERROR = 99


