# Iwdgui

Thanks for checking how you might be able to contribute iwdgui.
Please review the below chapters for ways how you can help.

For the further development of iwdgui, the following guidelines apply:

- Station mode will be developed further first, making it more feature rich,
  and look better
- Other modes like access point, and ad-hoc may follow later
- There is no intention to develop features for wired mode yet


# Ways to contribute

## Use and test it

When you find something strange then please log an [issue](https://gitlab.com/hfernh/iwdgui/-/issues).
Also if you find a missing feature then again, please log an issue.
Please mark any issue as either an enhancement or a bug.

## Spread the word

The more people use iwdgui, the more ideas we will get how to improve it.
If you like iwdgui then blog about, discuss it in on-line forums,
or mention it where ever.

## Package it

Iwdgui is currently packaged for [PyPI](https://pypi.org/project/iwdgui),
[Gentoo's GURU](https://gitweb.gentoo.org/repo/proj/guru.git/tree/net-wireless/iwdgui),
and [Arch' AUR](https://aur.archlinux.org/packages/iwdgui).
If you can package iwdgui for other distros like Fedora, Ubuntu, Debian, etc
then this will be highly appreciated.

## Bug fixing

If you feel up to it and you are able to fix an issue, then please do not
hesitate and create a patch and attach it to the issue you fixed.



Thanks for helping!
